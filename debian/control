Source: golang-github-vividcortex-godaemon
Section: devel
Priority: optional
Maintainer: Debian Go Packaging Team <pkg-go-maintainers@lists.alioth.debian.org>
Uploaders: Eric Dorland <eric@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any
Standards-Version: 4.6.0.0
Rules-Requires-Root: no
Homepage: https://github.com/VividCortex/godaemon
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-vividcortex-godaemon
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-vividcortex-godaemon.git
XS-Go-Import-Path: github.com/VividCortex/godaemon
Testsuite: autopkgtest-pkg-go

Package: golang-github-vividcortex-godaemon-dev
Architecture: all
Multi-Arch: foreign
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: Daemonize Go applications deviously
 Daemonize Go applications with exec() instead of fork().
 .
 You can't daemonize the usual way in Go. Daemonizing is a Unix concept
 that requires some specific things (http://goo.gl/vTUsVy) you can't do
 easily in Go. But you can still accomplish the same goals if you don't
 mind that your program will start copies of itself several times, as
 opposed to using fork() the way many programmers are accustomed to doing.
